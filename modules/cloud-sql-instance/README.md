### Cloud SQL instance 

Creates a Google Cloud SQL instance. Supports failover node creation.

##### Resources

- google_sql_database_instance

##### Usage

###### Basic / minimal

```hcl
module "sql_instance" {
  source = "modules/cloud-sql-instance"
  
  name             = "testing-mysql"
  region           = "europe-west3"
  tier             = "db-f1-micro"
  disk_size        = "100"
  maintenance_day  = "7"
  maintenance_hour = "17"
}
```

###### Advanced

```hcl
module "cloud-sql" {
  source = "modules/cloud-sql-instance"

  name             = "prd-mysql"
  region           = "europe-west3"
  tier             = "db-n1-standard-8"
  disk_size        = "200"
  maintenance_day  = "7"
  maintenance_hour = "17"
}

module "cloud-sql-failover" {
  source = "modules/cloud-sql-instance"

  name             = "prd-mysql-failover"
  region           = "europe-west3"
  tier             = "db-n1-standard-8"
  disk_size        = "200"
  maintenance_day  = "7"
  maintenance_hour = "21"

  failover_node        = true
  master_instance_name = "${module.cloud-sql.name}"
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the cluster, unique within the project and zone. | string | - | yes |
| region | The region to create certain resources in. | string | - | yes |
| tier | The machine type to use for the database instance. | string | - | yes |
| disk_size | The disk size for the database in gigabytes. | string | - | yes |
| maintenance_day | The day of week (1-7), starting on Monday. | string | - | yes |
| maintenance_hour | The hour of day (0-23), ignored if day not set. The timezone is UTC. | string | - | yes |
| master_instance_name | The name of the instance that will act as the master in the replication setup. | string | - | no |
| database_version | The MySQL version to use. Can be MYSQL_5_6, MYSQL_5_7 or POSTGRES_9_6. | string | MYSQL_5_7 | no |
| disk_autoresize | Configuration to increase storage size automatically. | boolean | true | no |  
| authorized_networks | A list of whitelisted IP addresses. | list | Cleversoft IPs | no |
| flags | A list of database flags to pass to the instance. | list | - | no |
| failover_node | Specifies if the replica is the failover target. | boolean | false | no |
| backup_start_time | HH:MM format time indicating when backup configuration starts. | string | 02:00 | no |
| disk_type | The type of data disk: PD_SSD or PD_HDD. | string | PD_SSD | no |

## Outputs

| Name | Description |
|------|-------------|
| name | Returns the name of the database instance. |
| region | Returns the region the database instance was created in. |

## Locals

| Name | Description |
|------|-------------|
| failover_replica_configuration | Both locals are used to specify if a database instance is a failover_node. No input from the user is required. |
| failover_value | Both locals are used to specify if a database instance is a failover_node. No input from the user is required. |

## Additional Info 

- Author: Maximilian Dorner
