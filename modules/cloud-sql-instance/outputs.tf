output "name" {
  value = "${google_sql_database_instance.mysql.name}"
}

output "region" {
  value = "${google_sql_database_instance.mysql.region}"
}