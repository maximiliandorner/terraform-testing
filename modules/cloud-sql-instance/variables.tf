variable "name" {
  description = "The name of the cluster, unique within the project and zone."
}

variable "region" {
  description = "The region to create certain resources in."
}

variable "tier" {
  description = "The machine type to use for the database instance."
}

variable "disk_size" {
  description = "The disk size for the database in gigabytes."
}

## The following two vars configure a maintenance window when an instance can automatically restart to apply updates

variable "maintenance_day" {
  description = "The day of week (1-7), starting on Monday."
}

variable "maintenance_hour" {
  description = "The hour of day (0-23), ignored if day not set. The timezone is UTC."
}

variable "master_instance_name" {
  description = "The name of the instance that will act as the master in the replication setup."
  default = ""
}

variable "database_version" {
  description = "The MySQL version to use. Can be MYSQL_5_6, MYSQL_5_7 or POSTGRES_9_6."
  default = "MYSQL_5_7"
}

variable "disk_autoresize" {
  description = "Configuration to increase storage size automatically."
  default = true
}

variable "authorized_networks" {
  description = "A list of whitelisted IP addresses."
  type = "list"
  default = [
    { name = "Cleversoft office Munich", value = "37.148.137.142/32" },
    { name = "Cleversoft office Sofia", value = "91.92.21.83/32" },
    { name = "Telehouse Servers", value = "91.92.21.83/32" },
    { name = "DDB Home", value = "79.219.57.127/32" },
    { name = "Alin Home", value = "89.15.39.201/32" },
  ]
}

variable "flags" {
  description = "A list of database flags to pass to the instance."
  type = "list"
  default = []
}

variable "failover_node" {
  description = "Specifies if the replica is the failover target."
  default = false
}

variable "backup_start_time" {
  description = "HH:MM format time indicating when backup configuration starts. Probably in UTC?"
  default = "02:00"
}

variable "disk_type" {
  description = "The type of data disk: PD_SSD or PD_HDD."
  default = "PD_SSD"
}

locals {
  failover_replica_configuration = [{
    failover_target = true
  }]
  # workaround for producing maps using conditionals: https://github.com/hashicorp/terraform/issues/12453
  failover_value = "${slice(local.failover_replica_configuration, 0, var.failover_node == 1 ? 1 : 0)}"
}
