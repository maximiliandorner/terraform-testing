resource "google_sql_database_instance" "mysql" {
  name = "${var.name}"
  region = "${var.region}"
  database_version = "${var.database_version}"
  master_instance_name = "${var.master_instance_name}"

  settings {
    tier = "${var.tier}"
    disk_size = "${var.disk_size}"
    disk_type = "${var.disk_type}"
    disk_autoresize = "${var.disk_autoresize}"

    database_flags = "${var.flags}"

    backup_configuration {
      binary_log_enabled = "${1 - var.failover_node}"
      enabled = "${1 - var.failover_node}"
      start_time = "${var.backup_start_time}"
    }

    ip_configuration {
      ipv4_enabled = true
      authorized_networks = "${var.authorized_networks}"
    }
    maintenance_window {
      day = "${var.maintenance_day}"
      hour = "${var.maintenance_hour}"
    }
  }

  replica_configuration = "${local.failover_value}"
}