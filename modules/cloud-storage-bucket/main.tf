resource "google_storage_bucket" "storage" {
  name     = "${var.purpose}-${var.prefix}"
  location = "${var.region}"
  storage_class = "${var.storage_class}"
  force_destroy = "${var.force_destroy}"
}