### Cloud storage bucket 

Creates a Google Cloud storage bucket.

##### Resources

- google_storage_bucket

##### Usage

###### Basic / minimal

```hcl
module "backup-bucket" {
  source = "modules/cloud-storage-bucket"

  prefix = "production"
  purpose = "backup"
  region = "europe-west3"
  force_destroy = "true"
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| region | The region to create certain resources in. | string | - | yes |
| prefix | The prefix to create resource names from. | string | - | yes |
| purpose | A short description of what the bucket will be used for. Attention, spaces are not allowed. | string | - | yes |
| storage_class | The Storage Class of the new bucket. Supported values: MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE. | string | REGIONAL | no |
| force_destroy | If true, Terraform will delete the Google Cloud Storage Bucket even if it's non-empty. | boolean | false | no |

## Outputs

| Name | Description |
|------|-------------|
| name | Returns the name of the storage bucket. |

## Additional Info 

- Author: Maximilian Dorner
