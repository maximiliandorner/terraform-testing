variable "region" {
  description = "The region to create certain resources in."
}

variable "prefix" {
  description = "The prefix to create resource names from."
}

variable "purpose" {
  description = "A short description of what the bucket will be used for. Attention, spaces are not allowed."
}

variable "storage_class" {
  description = "The Storage Class of the new bucket. Supported values: MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE."
  default = "REGIONAL"
}

variable "force_destroy" {
  description = "If true, Terraform will delete the Google Cloud Storage Bucket even if it's non-empty."
  default = false
}