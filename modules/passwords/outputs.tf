output passwords {
  value = "${data.external.passwords.result}"
  sensitive = true
}