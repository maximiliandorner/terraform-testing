variable "filename" {
  description = "The name of the passwords file in the filesystem."
  type = "string"
}