### Passwords

Uses [SOPS](https://github.com/mozilla/sops) to decrypt a json file containing the passwords.

##### Data

- external

##### Required software

- [terraform](https://www.terraform.io/)
- [sops](https://github.com/mozilla/sops)
- Configured application defaults for gcloud

##### Usage

```hcl
module "passwords" {
  source = "modules/passwords"
  filename = "./passwords.json"
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| filename | The name of the passwords file in the filesystem. | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| passwords | Returns the decrypted passwords as a [string->string] map. |

## Additional Info 

- Author: Dominik del Bondio
- Assistant: Maximilian Dorner